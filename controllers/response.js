// function _ResponseGetShipment(data, messenger) {
//     return {
//         "messenger": messenger,
//         "data": data ? data : {}
//     };
// }

// function _ResponseQuote(data = {}) {
//     return {
//         "data": data ? data : {},
//     };
// };


// function _ResponseShipment(data = {}) {
//     return {
//         "data": data ? data : {},
//     };
// };


module.exports = {
    _ResponseGetShipment: function (data, messenger) {
            return {
                "messenger": messenger,
                "data": data ? data : {}
            }
    },

    _ResponseQuote: function (data = {}) {
        return {
            "data": data ? data : {},
        };
    },

    _ResponseShipment: function (data = {}) {
        return {
            "data": data ? data : {},
        };
    }
};