const fs = require("fs");
const bodyParser = require('body-parser')

const ShipmentModel = require('../models/shipment');
const RateModel = require('../models/rate');
const Response = require('./response');




module.exports.getQuote = (req, res) => {
    const quoteBody = req.body.data;
    const quoteWeight = _getWeight(quoteBody.package.grossWeight.amount);
    RateModel.findOne({weight: {$lte: quoteWeight}}, ['_id', 'price'], {
        skip: 0,
        limit: 1,
        sort: {weight: -1}
    }, (err, data) => {
         var data = data ? data: {};
         var dataAmount;
         dataAmount =  [{
                        "id": data['_id'],
                        "amount": data['price']  
                        }];
        res.json(Response._ResponseQuote(dataAmount));
    });
};

module.exports.updateShipment = (req, res) => {
    const shipmentBody = req.body.data;
    const ref = req.params.ref;
    ShipmentModel.findOneAndUpdate({ref:ref}, {$set:shipmentBody},{upsert:true}, (err, dataCreate) => {
        if (err){
            console.log(`Failed to update`);
        }else {
             var dataAdd = dataCreate ? dataCreate : {};

             var responseUpdate = {
                 
                 "create_at": getDate(),
             }
             res.setHeader('Access-Control-Allow-Origin', '*')
             res.json(Response._ResponseShipment(responseUpdate));

        }
    });
}


module.exports.createShipment = (req, res) => {
try {
    const shipmentBody = req.body.data;
    const weightShipment = _getWeight(shipmentBody.package.grossWeight.amount);
    RateModel.findOne({weight: {$lte: weightShipment}}, ['_id', 'price'], {
        skip: 0,
        limit:1,
        sort: {weight: -1}
    }, (err, data) => {
         if (err) {
             res.json(Response._ResponseShipment({}));
         }
         else{
            ;
             data = data ?  data : {};
             shipmentBody.cost = data.price;
             shipmentBody.ref = _refGenerator(10);
             shipmentBody.created_at = getDate();
             ShipmentModel.create(shipmentBody, (err, dataCreate) => {
                 var dataAdd = dataCreate ? dataCreate : {};
                 var responseAdd = {
                     "ref": dataAdd.ref,
                     "created_at": getDate(),
                     "cost": dataAdd.cost
                 };                 
                 res.json(Response._ResponseShipment(responseAdd));
             });
         }
    });
 } catch(e) {
     res.json(Response._ResponseShipment({}));
 }
} 


module.exports.getShipments = (req, res) => {
    ShipmentModel.find({}, [], {skip: 0, limit: 10, sort:{create_at: 1}}, (err,data) => {
        if(err){
            res.json(Response._ResponseGetShipment({},'Failed to retrieve data'))
        }
        else{
            res.json(Response._ResponseGetShipment(data,'Successfully'));
        }
    });
    
};

module.exports.getShipmentDetail = (req, res) => {
    const ref = req.params.ref;
    ShipmentModel.findOne({ref: ref}, (err,data) => {
        if (err){
            res.json(Response._ResponseGetShipment({},'Shipment not found'))
        }
        else{
            res.json(Response._ResponseGetShipment(data, 'Shipment found'));
        }
    })
}



module.exports.deleteShipment = (req, res) => {
    const ref = req.params.ref;
    ShipmentModel.findOneAndDelete({ref: ref}, (err, data) => {
        if(err) {
            res.json({
                "status": "ERROR",
                "message": "Failed to delete" 
            });
        }
           else {
               res.json({
                   "status": "OK",
                   "message": "Shipment has been deleted"
               });
           }
    });

};




function _getWeight(weight = 0) {
    Number(weight);
    if (weight < 1) {
        weight = 250;
    }
    ;
    if (weight > 15000) {
        weight = 15001;
    }
    ;
    return weight;
}


function _refGenerator(length = 10) {    
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
};


function getDate() {
    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1; //January is 0!

    let yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    let formattedToday = `${dd}/${mm}/${yyyy}`;

    return formattedToday;
}