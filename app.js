const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const jsonwebtoken = require("jsonwebtoken");
const mongoose = require('mongoose');
const User = require('./models/user'),


app = express();
app.use(cors());

app.use(function (req, res, next) {
	if (req.headers && req.headers.authorization && req.headers.authorization.split(' ')[0] === 'JWT') {
		jsonwebtoken.verify(req.headers.authorization.split(' ')[1], 'RESTFULAPIs', function (err, decode) {
			if (err) req.user = undefined;
			req.user = decode;
			next();
		});
	} else {
		req.user = undefined;
		next();
	}
});

require('./routes/getquote.route')(app);
app.use(bodyParser.json());
app.use((request, response, next) => {
	response.setHeader('Access-Control-Allow-Origin', '*');
	response.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
	response.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");

	next();
});


mongoose.connect('mongodb://localhost:27017/finalproject', { useNewUrlParser: true });
mongoose.set('useFindAndModify', false);
// get reference to database
const connection = mongoose.connection;

connection.on('error', console.error.bind(console, 'connection error:'));

connection.once('open', () => {
    console.log('Mongo DB connections');
});


const server = app.listen(3000, () => {
	const hosts = server.address().address;
	const ports = server.address().port;

	console.log("App listening at http://%s:%s", hosts, ports);
});



