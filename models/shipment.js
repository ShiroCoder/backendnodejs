const mongoose = require('mongoose');

let shipmentSchema = new mongoose.Schema({
        
     
           quote: {
            id: {type: String},
        },
          cost: Number,
          origin: {
            contact: {
              name: String,
              email: String,
              phone: String ,
            },
            address: {
              country_code: String,
              locality: String,
              postal_code: {type: String, default: 0},
              address_line1: String 
            }
          },
          destination: {
            contact: {
              name: String,
              email: String,
              phone: String,
            },
            address: {
              country_code: String,
              locality: String,
              postal_code: {type: String, default: 0},
              address_line1:  String
          }     
         },
         package: {
              dimensions: {
                 height: { type: Number},
                 width: { type: Number},
                 length: { type: Number},
                 unit: String	
          },
          grossWeight: {
             amount: {type: Number, default: 0} ,
             unit: String,
          }
             
        },
             
        created_at : { type : String },
             ref :  { type: String},
             cost :  { type: String},
          }
        
  );

module.exports = mongoose.model('Shipment', shipmentSchema);