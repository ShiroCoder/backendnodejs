const mongoose = require('mongoose');


let rateSchema = new mongoose.Schema({
   weight: {
       type: Number, unique: true,
       required: true
   },
   price: {
       type: Number,
       required: true
   },
   from_country: {
       type: String,
       required: true
   },
   to_country:{ 
       type:  String,
       required: true
   }
});


module.exports = mongoose.model('Rate', rateSchema, 'rate');
