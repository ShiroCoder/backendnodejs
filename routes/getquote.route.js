
const bodyParser = require('body-parser');
const userHandlers = require('../controllers/user.controller');

const jsonParser = bodyParser.json();
module.exports = (app) => {
    
const controller = require('../controllers/getquote.controller');
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });


app.post('/auth/register', jsonParser, userHandlers.register);

app.post('/auth/sign_in', jsonParser,userHandlers.sign_in);

app.post('/getquote', jsonParser, controller.getQuote);

app.post('/createshipment', jsonParser, controller.createShipment);

app.patch(`/updateshipment/:ref`, jsonParser, controller.updateShipment);

app.get('/getshipment', jsonParser, controller.getShipments);

app.get('/getshipmentdetail/:ref', jsonParser, controller.getShipmentDetail);

app.delete('/deleteshipment/:ref', jsonParser, controller.deleteShipment);
}